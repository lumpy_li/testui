
# HYCAN App/小程序交互设计规范

### 使用方法
你可以通过订阅的方式来订阅此组件库，或者直接克隆本项目到本地，用 [Sketch][1] 导入此组件库。
- [订阅地址][2]

此外推荐使用 SF Symbols 来搭配使用，组件库已在导航栏、表视图中加入支持 SF Symbols 的控件，可以直接输入来显示对应的图标。
- [SF Symbols 的介绍][3]
- [下载 SF Symbols][4]

### 前言
为了让 HYCAN 的交互设计更为统一、易用，根据现有设计，整理并搭建一套符合 HYCAN 设计规范的交互控件库 HYCAN WireframeKit，并将持续更新和优化，以适应项目的发展。

所有组件即拖即用，适用于快速创建中/高保真线框原型。

### 目录
![][image-1]

组件库包含以下模块，并已按不同类别进行了分类：

#### Styles · 样式
- Color · 颜色
- Font  · 字体

#### Bars · 栏
- Navigation Bar · 导航栏 
- Navigation Bar · 导航栏（小程序）
- Tabs · 切换器
- Modal · 模态导航栏
- Modal Stack · 模态堆栈（iOS）
- Tool Bar · 工具栏
- Notice Bar · 通告栏
- Search Bar · 搜索框

#### Table Views · 表视图
- Cell · 表单
- Text Field · 输入框
- Menu · 菜单选项（Android）

#### Views · 视图
- Alert · 对话框
- Action Sheet · 动作菜单
- Share Sheet · 分享菜单
- Toast · 反馈提示
- Popup · 浮层
- AD Popup · 广告弹窗

#### Control · 控制
- Button · 按钮
- Stepper · 计步器
- Switch · 开关
- Progress Indicator · 进度反馈
- Slider · 滑杆
- Picker · 选择器
- Floating Action Button · 浮动操作按钮

#### Pages · 页面
- Empty Page · 缺省页面
- Bad Network Page · 无网络页面

#### Icons · 图标
- Icons · 常用图标

### 样式
- **颜色**：提供 12 种颜色定义，从黑色到白色全透明，供不同类型的文本及填充、描边样式使用。颜色名称全局统一。
- **外观**：提供 52 种样式定义，包括实色填充（命名规则：Fill-颜色名称）、空心描边（命名规则：Border-颜色名称）、实色及多方向描边等 （命名规则：Fill-颜色名称-Border-颜色名称）。
- **字形**：基于 3 种对齐方式、2 种字体重量、6 种字色、8 种字号，提供 288 种文字风格定义 (命名规则：对齐方式-字体重量-颜色名称-字号)。

![][image-2]

### 栏
![][image-3]
![][image-4]




[1]:	https://www.sketch.com
[2]:	sketch://add-library?url=https://design-lumpy.oss-cn-hangzhou.aliyuncs.com/SketchLibrary/sketch.xml
[3]:	https://developer.apple.com/design/human-interface-guidelines/sf-symbols/overview/
[4]:	https://developer.apple.com/design/downloads/SF-Symbols.dmg

[image-1]:	https://gitlab.com/lumpy_li/testui/raw/master/%E8%AE%BE%E8%AE%A1%E8%A7%84%E8%8C%83/%E4%BA%A4%E4%BA%92%E8%A7%84%E8%8C%83/Images/Categoly.png
[image-2]:	https://gitlab.com/lumpy_li/testui/raw/master/%E8%AE%BE%E8%AE%A1%E8%A7%84%E8%8C%83/%E4%BA%A4%E4%BA%92%E8%A7%84%E8%8C%83/Images/Styles/Styles.png
[image-3]:	https://gitlab.com/lumpy_li/testui/raw/master/%E8%AE%BE%E8%AE%A1%E8%A7%84%E8%8C%83/%E4%BA%A4%E4%BA%92%E8%A7%84%E8%8C%83/Images/Bars/%E7%8A%B6%E6%80%81%E6%A0%8F%20&%20%E4%B8%BB%E9%A1%B5%E6%8C%87%E6%A0%87.png
[image-4]:	https://gitlab.com/lumpy_li/testui/raw/master/%E8%AE%BE%E8%AE%A1%E8%A7%84%E8%8C%83/%E4%BA%A4%E4%BA%92%E8%A7%84%E8%8C%83/Images/Bars/%E5%AF%BC%E8%88%AA%E6%A0%8F.png